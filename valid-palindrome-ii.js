/**
 * 2019.10.09 validPalindrome II
 * https://leetcode.com/problems/valid-palindrome-ii/
 *
 * come up by myself but not efficient
 *
 */

/**
 * @param {string} s
 * @return {boolean}
 */
var validPalindrome = function(s) {
  if (s.length === 0) {
    return false;
  }
  if (s.length === 1) {
    return true;
  }
  let deleteCount = 0;

  const dfs = (i, j) => {
    while (i <= j) {
      if (i === j) {
        return true;
      }
      if (s[i] === s[j]) {
        ++i;
        --j;
        continue;
      }
      if (i !== j) {
        if (deleteCount >= 1) {
          return false;
        }
        // console.log(`i === j - 1`, i === j - 1);
        if (i === j - 1) {
          return true;
        }
        let result1;
        let result2;
        if (s[i] === s[j - 1]) {
          ++deleteCount;
          result1 = dfs(i, j - 1);
        }
        if (s[i + 1] === s[j]) {
          ++deleteCount;
          result2 = dfs(i + 1, j);
        }
        if (result1 === true || result2 === true) {
          return true;
        }

        return false;
      }
    }
    return true;
  };
  return dfs(0, s.length - 1);
};

console.log(validPalindrome('bddb'));
