/**
 *
 * 2019.09.30 18:32
 * DFS version of removeStones
 * refers to
 * https://leetcode.com/problems/most-stones-removed-with-same-row-or-column/discuss/205573/JavaScript-Union-Find-%2B-DFS
 */

var removeStones = function(stones) {
  let rows = new Map();
  let columns = new Map();

  stones.map(([x, y]) => {
    if (!rows.has(y)) {
      rows.set(y, [x]);
    } else {
      rows.set(y, rows.get(y).concat(x));
    }

    if (!columns.has(x)) {
      columns.set(x, [y]);
    } else {
    }
    columns.set(x, columns.get(x).concat(y));
  });
  let visited = new Map();
  let count = 0;
  const dfs = (x, y) => {
    const str = `${x},${y}`;

    if (!visited.has(str)) {
      visited.set(`${x},${y}`, true);
      for (let yy of columns.get(x)) {
        dfs(x, yy);
      }
      for (let xx of rows.get(y)) {
        dfs(xx, y);
      }
    }
  };
  stones.map(([x, y]) => {
    const str = `${x},${y}`;
    if (!visited.has(str)) {
      dfs(x, y);
      ++count;
    }
  });
  return stones.length - count;
};

const stones = [[0, 0], [0, 1], [1, 0], [1, 2], [2, 1], [2, 2]];
console.log(`output ${removeStones(stones)}`);
