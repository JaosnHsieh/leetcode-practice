/**
 * 2019.10.14 come up with my head
 */
var removeDuplicates = function(nums) {
  let hash = {};
  let i = 0;
  let j = 0;

  while (j < nums.length) {
    if (hash[nums[j]] === undefined) {
      hash[nums[j]] = 1;
      nums[i] = nums[j];
      ++i;
      ++j;
    } else {
      ++j;
    }
  }
  return i;
};
console.log(removeDuplicates([1, 1, 2]));
