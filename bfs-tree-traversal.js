/**
 * 2018.11.15 BFS level order binary tree taversal
 * refer to https://www.geeksforgeeks.org/level-order-tree-traversal
 */

class Q {
    constructor(arr = []) {
      this.data = [...arr];
    }
    enqueue(d) {
      this.data.unshift(d);
    }
    dequque(d) {
      return this.data.pop(d);
    }
    isEmpty() {
      return this.data.length === 0;
    }
  }
  
  class Node {
    constructor(data, left, right) {
      this.data = data;
      this.left = left;
      this.right = right;
    }
  }
  
  class BinaryTree {
    constructor() {
      this.root;
    }
    printLevelOrder() {
      const q = new Q();
      q.enqueue(this.root);
  
      while (!q.isEmpty()) {
        const currentNode = q.dequque();
  
        if (currentNode.left) {
          q.enqueue(currentNode.left);
        }
        if (currentNode.right) {
          q.enqueue(currentNode.right);
        }
      }
    }
  }
  
  const bt = new BinaryTree();
  bt.root = new Node(1);
  bt.root.left = new Node(2);
  bt.root.right = new Node(3);
  bt.root.left.left = new Node(4);
  bt.root.left.right = new Node(5);
  
  // console.log(JSON.stringify(bt, 0, 2));
  bt.printLevelOrder();
  