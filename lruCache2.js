/**
 * 2019.07.13 lru cache leetcode
 * refers to
 * https://www.youtube.com/watch?v=S6IfqDXWa10
 * https://github.com/bephrem1/backtobackswe/blob/master/Linked%20Lists/LRUCache.java
 *
 * dummy head and dummy tail really save a lot of codes !!
 */

class Node {
  constructor({ prev, next, value, key }) {
    this.prev = prev;
    this.next = next;
    this.value = value;
    this.key = key;
  }
}

let count = 0;
class LRUCache {
  constructor(limit) {
    this.limit = limit;
    this.size = 0;
    this.hash = {};
    this.head = new Node({ value: 0 });
    this.tail = new Node({ value: 0 });
    this.head.next = this.tail;
    this.tail.prev = this.head;
  }
  get(key) {
    const node = this.hash[key];
    if (node === undefined) {
      return -1;
    } else {
      const value = node.value;

      node.prev.next = node.next;
      node.next.prev = node.prev;

      node.next = this.head.next;
      node.prev = this.head;
      this.head.next.prev = node;
      this.head.next = node;

      return value;
    }
  }

  put(key, value) {
    const node = this.hash[key];
    if (node === undefined) {
      const newNode = new Node({
        key,
        value,
        prev: this.head,
        next: this.head.next,
      });
      this.head.next.prev = newNode;
      this.head.next = newNode;
      this.hash[key] = newNode;
      ++this.size;
      if (this.size > this.limit) {
        this.remove(this.tail.prev.key);
      }
    } else {
      node.value = value;

      node.prev.next = node.next;
      node.next.prev = node.prev;

      node.prev = this.head;
      node.next = this.head.next;

      this.head.next.prev = node;
      this.head.next = node;
    }
  }

  traverse() {
    console.log('this.hash', this.hash);
    let current = this.head.next;
    while (current !== this.tail) {
      console.log(current);
      current = current.next;
    }
  }

  remove(key) {
    const node = this.hash[key];
    node.prev.next = node.next;
    node.next.prev = node.prev;
    delete this.hash[key];
    --this.size;
  }
}

const cache = new LRUCache(1);
cache.put(2, 1);
// cache.put(2, 2);
// cache.put(3, 3);
// cache.put(2, 2);
cache.get(2);
cache.traverse();
// console.log('reuslts', results);
// cache.tranverse();
// console.log('cache.cache', cache.cache);
