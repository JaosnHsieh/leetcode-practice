/** 2018.01.16
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {
    if( strs.length === 0){
        return "";
    }
    if( strs.length === 1){
        return strs[0];
    }

    var same = strs[0];
    
    for( var i = 1 ; i < strs.length ; ++i ){
        var str = strs[i];

        var j = 0
        for( ; j < str.length ; ++j ){
            if(same[j] !== str[j]){
                break;
            }
        }
        same = str.slice(0,j);
    }
    return same;
};

console.log(longestCommonPrefix(['aaa', 'aaab','aaaaa']));
