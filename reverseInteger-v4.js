/** 2018.04.10 目前效率最高的解 可是覺得通用性不高 只是利用了數學的特性
 * refer to http://bangbingsyb.blogspot.tw/2014/11/leetcode-reverse-integer.html
 *
 * @param {number} x
 * @return {number}
 */
var reverse = function(x) {
  var INT_MAX = Math.pow(2, 31) - 1;
  let ret = 0;
  while (x != 0) {
    if (ret > INT_MAX || ret < -(INT_MAX + 1)) {
      return 0;
    }
    ret = ret * 10 + x % 10;
    x = x > 0 ? Math.floor(x / 10) : Math.ceil(x / 10);
  }
  if (ret > INT_MAX || ret < -(INT_MAX + 1)) {
    return 0;
  }
  return ret;
};
