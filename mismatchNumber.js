/**
 *
 * 2019.10.12
 * https://leetcode.com/problems/set-mismatch/submissions/
 * https://www.cnblogs.com/grandyang/p/7324242.html
 */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var findErrorNums = function(nums) {
  const hash = {};
  let ans = [];

  for (let n of nums) {
    if (hash[n] === undefined) {
      hash[n] === undefined;
      hash[n] = 1;
    } else {
      hash[n] += 1;
    }
  }
  for (let i = 0; i < nums.length; ++i) {
    if (hash[i + 1] === 2) {
      ans[0] = i + 1;
    }
    if (hash[i + 1] === undefined) {
      ans[1] = i + 1;
    }
  }
  return ans;
};

console.log(findErrorNums([1, 1]));
