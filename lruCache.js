/**
 * 2019.07.05 for the requirements of LRU
 * most of code refers to
 * https://medium.com/dev-blogs/ds-with-js-linked-lists-ii-3b387596e27e
 */

class Node {
  constructor({ data = null, prev = null, next = null } = {}) {
    this.data = data;
    this.prev = prev;
    this.next = next;
  }
}

class DoubledLinkedList {
  constructor(data) {
    if (data) {
      let node = new Node({ data });
      this.head = node;
      this.tail = node;
    } else {
      this.head = null;
      this.tail = null;
    }
  }
  append(data) {
    let node = new Node({ data });
    if (!this.head) {
      this.head = node;
      this.tail = node;
    } else {
      node.prev = this.tail;

      this.tail.next = node;
      this.tail = node;
    }
  }
  appendAt(pos, data) {
    let node = new Node({ data });
    let current = this.head;
    let currentPos = 0;
    if (pos === currentPos) {
      if (this.head) {
        this.head.prev = node;
      }
      if (!this.tail) {
        this.tail = node;
      }
      node.next = this.head;
      this.head = node;
    } else {
      while (current) {
        ++currentPos;
        current = current.next;
        if (currentPos === pos) {
          if (current === null) {
            node.prev = this.tail;
            this.tail.next = node;
            this.tail = node;
          } else {
            node.prev = current.prev;
            node.next = current;
            current.prev.next = node;
            current.prev = node;
          }
        }
      }
    }
    return node;
  }
  size() {
    let current = this.head;
    let size = 0;
    while (current) {
      current = current.next;
      ++size;
    }
    return size;
  }
  display() {
    let current = this.head;
    let elements = [];
    if (this.head === null) {
      console.log('empty');
    } else {
      while (current.next !== null) {
        elements.push(current.data);
        current = current.next;
      }
      elements.push(current.data);
    }
    console.log(elements.join(' '));
  }

  traverse() {
    let current = this.head;
    let elements = [];
    if (this.head === null) {
      console.log('empty');
    } else {
      while (current.next !== null) {
        elements.push(current);
        current = current.next;
      }
      elements.push(current);
    }
    console.log(elements);
  }

  remove(data) {
    let current = this.head;
    while (current) {
      if (current.data === data) {
        if (current === this.head && current === this.tail) {
          this.head = null;
          this.tail = null;
        } else if (current === this.head) {
          this.head = current.next;
        } else if (current === this.tail) {
          this.tail = current.prev;
        } else {
          current.prev.next = current.next;
          current.next.prev = current.prev;
        }
      }
      current = current.next;
    }
  }

  removeAt(pos) {
    let current = this.head;
    let currentPos = 0;
    while (current) {
      ++currentPos;
      current = current.next;
      if (pos === 0) {
        this.head = current;
        this.head.prev = null;
        return true;
      } else {
        if (currentPos === pos) {
          if (current === this.tail) {
            current.prev.next = null;
            return true;
          } else {
            current.next.prev = current.prev;
            current.prev.next = current.next;
            return true;
          }
        }
      }
    }
    return false;
  }
}

/**
 * @param {number} capacity
 */
var LRUCache = function(capacity) {
  this.capacity = capacity;
  this.hash = {};
  this.ll = new DoubledLinkedList();
};

/**
 * @param {number} key
 * @return {number}
 */
LRUCache.prototype.get = function(key) {
  if (this.hash[key]) {
    const oldHead = this.ll.head;
    if (oldHead.next === this.hash[key]) {
      oldHead.next = null;
      this.ll.tail = oldHead;
    }
    oldHead.prev = this.hash[key];
    this.ll.head = this.hash[key];
    this.ll.head.prev = null;
    this.ll.head.next = oldHead;
    // this.ll.traverse();
    return this.hash[key].data;
  } else {
    return -1;
  }
};

/**
 * @param {number} key
 * @param {number} value
 * @return {void}
 */
LRUCache.prototype.put = function(key, value) {
  if (this.hash[key]) {
    this.hash[key].prev.next = this.hash[key].next;
    this.hash[key].prev = null;
    this.hash[key].next = this.ll.head;
    this.ll.head.prev = this.hash[key];
    this.ll.head = this.hash[key];
  } else {
    const node = this.ll.appendAt(0, value);
    // console.log(`this.ll.tail`, this.ll.tail);
    this.hash[key] = node;
    // console.log('this.ll.size', this.ll.size());
    // console.log(`this.ll.size > this.capacity`, this.ll.size() > this.capacity);
    if (this.ll.size() > this.capacity) {
      //   console.log('this.tail', this.ll.tail);
      Object.keys(this.hash).map(key => {
        if (this.hash[key] === this.ll.tail) {
          //   console.log(
          //     `this.hash[key] === this.ll.tail`,
          //     this.hash[key] === this.ll.tail,
          //   );
          //   console.log('this.hash before', this.hash);
          delete this.hash[key];
          //   console.log('this.hash after', this.hash);
        }
      });
      //   console.log(`this.ll.size()`, this.ll.size());
      // console.log(`this.ll.tail`, this.ll.tail);
      this.ll.tail.prev.next = null;
      this.ll.tail = this.ll.tail.prev;
    }
  }
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * var obj = new LRUCache(capacity)
 * var param_1 = obj.get(key)
 * obj.put(key,value)
 */

let cache = new LRUCache(2 /* capacity */);
// console.log('cache', cache);
cache.put(1, 1);
cache.put(2, 2);
// console.log('cache.hash', cache.hash);
// cache.ll.traverse();
// console.log('1...', cache.hash);
console.log(cache.get(1)); // returns 1
// console.log('2...', cache.hash);
// console.log('tail...', cache.ll.tail);
cache.put(3, 3); // evicts key 2
// console.log('3...', cache.hash);
// cache.ll.traverse();
console.log(cache.get(2)); // returns -1 (not found)
cache.put(4, 4); // evicts key 1
console.log(cache.get(1)); // returns -1 (not found)
console.log(cache.get(3)); // returns 3
console.log(cache.get(4)); // returns 4
