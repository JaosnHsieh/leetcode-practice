/** 2018.01.05
 * 參照 https://leetcode.com/problems/3sum/discuss/7399 複寫一遍
 * @param {number[]} nums
 * @return {number[][]}
 */

var threeSum = function (nums) {
    nums.sort(function (a, b) {
        return a - b;
    });
    var arr = [];
    console.log(nums);
    for (var i = 0; i < nums.length - 2; ++i) {
        if ( i > 0 && nums[i] === nums[i - 1]) {
            continue;
        }

        var l = i + 1;
        var r = nums.length - 1;
        // console.log(l);
        // console.log(r);
        while (l < r) {
            // console.log(nums[i]);
            // console.log(nums[l]);
            // console.log(nums[r]);
            // console.log(r);
            var sum = nums[i] + nums[l] + nums[r];
            // console.log(sum)
            if (sum === 0) {
                // console.log([nums[i], nums[l], nums[r]]);
                arr.push([nums[i], nums[l], nums[r]]);
                while (nums[l] === nums[l + 1]) ++l;
                while (nums[r] === nums[r - 1]) --r;
                ++l;
                --r;
            } else if (sum > 0) {
                --r;
            } else {
                ++l;
            }

        }
    }

    return arr;
};

var a = threeSum([-4,-2,-2,-2,0,1,2,2,2,3,3,4,4,6,6]);
console.log('a...', a);