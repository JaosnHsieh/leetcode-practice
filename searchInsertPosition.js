/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var searchInsert = function(nums, target) {
  const dfs = (min, max) => {
    if (min > max) {
      return min;
    }
    const middleIndex = Math.floor((min + max) / 2);
    const middleValue = nums[middleIndex];

    if (target < middleValue) {
      return dfs(min, middleIndex - 1);
    } else if (target > middleValue) {
      return dfs(middleIndex + 1, max);
    } else if (target === middleValue) {
      return middleIndex;
    }
  };
  return dfs(0, nums.length - 1);
};

console.log(searchInsert([1, 3, 5, 8], 6));
