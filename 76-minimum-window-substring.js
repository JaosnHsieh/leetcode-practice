//2020.09.05 refers to java solution here : https://leetcode.com/problems/minimum-window-substring/solution/
function minWindow(s, t) {
  let left = 0;
  let right = 0;
  let ans = '';
  let count = 0;
  let windowMap = {};
  let tMap = {};
  t.split('').map((c) => (tMap[c] = (tMap[c] || 0) + 1));

  for (; right < s.length; ++right) {
    const c = s[right];
    windowMap[c] = (windowMap[c] || 0) + 1;
    if (tMap[c] > 0 && tMap[c] >= windowMap[c]) {
      count++;
    }

    while (left <= right && count === t.length) {
      const lc = s[left];
      const candidateAns = s.substring(left, right + 1);
      if (ans === '') {
        ans = candidateAns;
      }
      if (candidateAns.length < ans.length) {
        ans = candidateAns;
      }

      windowMap[lc]--;
      if (tMap[lc] > 0 && windowMap[lc] < tMap[lc]) {
        count--;
      }

      left++;
    }
  }
  return ans;
}
