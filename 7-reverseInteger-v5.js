/**
 *
 * 2019.10.13
 * come up with similar idea from the reference.
 * refers to https://leetcode.com/problems/reverse-integer/discuss/4060/My-accepted-15-lines-of-code-for-Java
 */

/**
 * @param {number} x
 * @return {number}
 */
var reverse = function(x) {
  let result = 0;

  while (x !== 0) {
    result = result * 10 + (x % 10);
    // console.log(result);
    x = x > 0 ? Math.floor(x / 10) : Math.ceil(x / 10);
    // console.log(`x ${x}`);
  }
  //https://en.wikipedia.org/wiki/32-bit
  if (result > Math.pow(2, 31) - 1 || result < -Math.pow(2, 31)) {
    return 0;
  }
  return result;
};

console.log(reverse(-2147483412));
