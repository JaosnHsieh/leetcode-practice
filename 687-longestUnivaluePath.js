/**
 * 2019.10.13
 * refers to
 * https://www.youtube.com/watch?v=FdhiJufTI7c
 */

/**
 * @param {TreeNode} root
 * @return {number}
 */

var longestUnivaluePath = function(root) {
  if (!root) {
    return 0;
  }
  let max = 0;

  helper(root, root.val);
  return max;

  function helper(node, preVal) {
    if (!node) {
      return 0;
    }
    let left = helper(node.left, node.val);
    let right = helper(node.right, node.val);
    max = Math.max(max, left + right);
    if (node.val === preVal) {
      return Math.max(left, right) + 1;
    }
    return 0;
  }
};
console.log(
  longestUnivaluePath({
    val: 1,
    left: { val: 1 },
    right: {
      val: 1,
    },
  }),
);
