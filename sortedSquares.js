/**
 *
 * 2019.09.26 refers to https://leetcode.com/articles/squares-of-a-sorted-array/
 */
/**
 * @param {number[]} A
 * @return {number[]}
 */
var sortedSquares = function(A) {
  let results = [];
  let currentIndex = 0;
  let positiveFirstIndex = 0;
  let negativeEndIndex;

  while (A[positiveFirstIndex] < 0) {
    ++positiveFirstIndex;
  }

  negativeEndIndex = positiveFirstIndex - 1;

  while (negativeEndIndex >= 0 && positiveFirstIndex < A.length) {
    if (A[negativeEndIndex] * A[negativeEndIndex] < A[positiveFirstIndex] * A[positiveFirstIndex]) {
      results[currentIndex] = A[negativeEndIndex] * A[negativeEndIndex];
      --negativeEndIndex;
    } else {
      results[currentIndex] = A[positiveFirstIndex] * A[positiveFirstIndex];
      ++positiveFirstIndex;
    }
    ++currentIndex;
  }

  while (negativeEndIndex >= 0) {
    results[currentIndex] = A[negativeEndIndex] * A[negativeEndIndex];
    ++currentIndex;
    --negativeEndIndex;
  }
  while (positiveFirstIndex < A.length) {
    results[currentIndex] = A[positiveFirstIndex] * A[positiveFirstIndex];
    ++currentIndex;
    ++positiveFirstIndex;
  }
  return results;
};

console.log(`sortedSquares`, sortedSquares([-4, -1, 0, 3, 10]));
