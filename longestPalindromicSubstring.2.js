/** 2018.04.10 這個 Dynamic Programming 解法比  Expand Around Center (longestPalindromicSubstring.js) 慢
 *  refer to:
 *https://www.youtube.com/watch?v=obBdxeCx_Qs
 *https://www.geeksforgeeks.org/longest-palindrome-substring-set-1/
 * @param {string} s
 * @return {string}
 */
var longestPalindrome = function(s) {
  let length = s.length;
  if (length === 0) return "";
  let start = 0,
    maxLength = 1;
  let p = new Array(length);
  for (let i = 0; i < length; ++i) {
    p[i] = new Array(length);
  }
  // length = 1
  for (let i = 0; i < length; ++i) {
    p[i][i] = true;
  }
  // length = 2
  for (let i = 0; i < length - 1; ++i) {
    if (s[i] === s[i + 1]) {
      p[i][i + 1] = true;
      start = i;
      maxLength = 2;
    }
  }
  // length >= 3

  for (let k = 3; k <= length; ++k) {
    for (let i = 0; i < length - k + 1; ++i) {
      let j = i + k - 1;
      if (p[i + 1][j - 1] && s[i] === s[j]) {
        p[i][j] = true;
        if (k > maxLength) {
          maxLength = k;
          start = i;
        }
      }
    }
  }

  console.log(s.substring(start, start + maxLength));
};
longestPalindrome("babad");
