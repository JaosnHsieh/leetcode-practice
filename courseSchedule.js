/**
 * 2019.10.01
 * https://leetcode.com/problems/course-schedule/
 * refers to
 * https://www.youtube.com/watch?v=M6SBePBMznU
 * https://www.youtube.com/watch?v=Q9PIxaNGnig
 * https://leetcode.com/problems/course-schedule/discuss/146325/JavaScript-DFS
 */
/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {boolean}
 */
var canFinish = function(numCourses, prerequisites) {
  let dependenciesArr = Array.from({ length: numCourses }, e => []);

  prerequisites.map(([n, preN]) => {
    dependenciesArr[n].push(preN);
  });
  let visited = {};
  let visiting = {};
  //   let stack = [];
  for (let [n] of prerequisites) {
    if (dfs(n) === 2) {
      return false;
    }
  }
  return true;
  function dfs(n) {
    // console.log(`visiting[${n}]`, visiting);
    if (visiting[n]) {
      return 2;
    }
    if (visited[n]) {
      return 1;
    }

    visiting[n] = true;
    for (let i = 0; i < dependenciesArr[n].length; ++i) {
      if (!visited[dependenciesArr[n][i]]) {
        const result = dfs(dependenciesArr[n][i]);
        if (result === 2) {
          return 2;
        }
      }
    }
    delete visiting[n];
    visited[n] = true;
    // stack.push(n);
    return true;
  }
};

console.log(canFinish(3, [[1, 0], [2, 1]]));
