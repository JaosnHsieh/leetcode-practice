/** 2018.04.10
 * refer to https://skyyen999.gitbooks.io/-leetcode-with-javascript/content/questions/7md.html
 * @param {number} x
 * @return {number}
 */
var reverse = function(x) {
  var INT_MAX = Math.pow(2, 31) - 1;

  let reverseStr = x
    .toString()
    .split("")
    .reverse()
    .join("");
  if (reverseStr.charAt(reverseStr.length - 1) === "-") {
    reverseStr = "-" + reverseStr.slice(0, reverseStr.length - 1);
  }
  if (reverseStr > INT_MAX || reverseStr < -(INT_MAX - 1)) {
    return 0;
  } else {
    return parseInt(reverseStr);
  }
};
