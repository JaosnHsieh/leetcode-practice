const range = (start, stop, step) =>
  Array.from({ length: (stop - start) / step + 1 }, (_, i) => start + i * step);

const getCharCreator = (char = ' ') => (n = 0) =>
  range(0, n - 1, 1).reduce(accu => (accu += char), '');

const getBlank = getCharCreator(' ');
const getStar = getCharCreator('*');

let n = 7;

range(1, n, 1).map(currentLine => {
  let lineChar = '';
  lineChar += getBlank(n - currentLine);
  lineChar += getStar(currentLine * 2 - 1);
  console.log(lineChar);
});
range(1, n - 1, 1).map(currentLine => {
  let lineChar = '';
  lineChar += getBlank(currentLine);
  lineChar += getStar((n - currentLine) * 2 - 1);
  console.log(lineChar);
});
