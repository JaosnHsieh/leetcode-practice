/**
 * 2019.07.05 for the requirements of LRU
 * most of code refers to
 * https://medium.com/dev-blogs/ds-with-js-linked-lists-db5138ff139f
 */

class Node {
  constructor({ data = null, next = null } = {}) {
    this.data = data;
    this.next = next;
  }
}

class LinkedList {
  constructor(data) {
    this.head = new Node({ data });
  }
  append(data) {
    let current = this.head;
    let newNode = new Node({ data });
    while (current.next !== null) {
      current = current.next;
    }
    current.next = newNode;
  }
  display() {
    let current = this.head;
    let elements = [];
    while (current.next !== null) {
      elements.push(current.data);
      current = current.next;
    }
    elements.push(current.data);
    console.log(elements.join(' '));
  }

  remove(data) {
    let current = this.head;
    if (current.data === data) {
      this.head = current.next;
      return true;
    }
    while (current.next !== null) {
      let prev = current;
      current = current.next;
      if (current.data === data) {
        prev.next = current.next;
        return true;
      }
    }
    return false;
  }
}

const ll = new LinkedList(1);
ll.append(2);
ll.append(3);
ll.append(4);
ll.display();
console.log(ll.remove(2));
ll.display();
