/**
 * 2019.07.05 for the requirements of LRU
 * most of code refers to
 * https://medium.com/dev-blogs/ds-with-js-linked-lists-ii-3b387596e27e
 */

class Node {
  constructor({ data = null, prev = null, next = null } = {}) {
    this.data = data;
    this.prev = prev;
    this.next = next;
  }
}

class DoubledLinkedList {
  constructor(data) {
    if (data) {
      let node = new Node({ data });
      this.head = node;
      this.tail = node;
    } else {
      this.head = null;
      this.tail = null;
    }
  }
  append(data) {
    let node = new Node({ data });
    if (!this.head) {
      this.head = node;
      this.tail = node;
    } else {
      node.prev = this.tail;

      this.tail.next = node;
      this.tail = node;
    }
  }
  appendAt(pos, data) {
    let node = new Node({ data });
    let current = this.head;
    let currentPos = 0;
    if (pos === currentPos) {
      if (this.head) {
        this.head.prev = node;
      }
      if (!this.tail) {
        this.tail = node;
      }
      node.next = this.head;
      this.head = node;
    } else {
      while (current) {
        ++currentPos;
        current = current.next;
        if (currentPos === pos) {
          if (current === null) {
            node.prev = this.tail;
            this.tail.next = node;
            this.tail = node;
          } else {
            node.prev = current.prev;
            node.next = current;
            current.prev.next = node;
            current.prev = node;
          }
        }
      }
    }
    return node;
  }
  size() {
    let current = this.head;
    let size = 0;
    while (current) {
      current = current.next;
      ++size;
    }
    return size;
  }
  display() {
    let current = this.head;
    let elements = [];
    if (this.head === null) {
      console.log('empty');
    } else {
      while (current.next !== null) {
        elements.push(current.data);
        current = current.next;
      }
      elements.push(current.data);
    }
    console.log(elements.join(' '));
  }

  traverse() {
    let current = this.head;
    let elements = [];
    if (this.head === null) {
      console.log('empty');
    } else {
      while (current.next !== null) {
        elements.push(current);
        current = current.next;
      }
      elements.push(current);
    }
    console.log(elements);
  }

  remove(data) {
    let current = this.head;
    while (current) {
      if (current.data === data) {
        if (current === this.head && current === this.tail) {
          this.head = null;
          this.tail = null;
        } else if (current === this.head) {
          this.head = current.next;
        } else if (current === this.tail) {
          this.tail = current.prev;
        } else {
          current.prev.next = current.next;
          current.next.prev = current.prev;
        }
      }
      current = current.next;
    }
  }

  removeAt(pos) {
    let current = this.head;
    let currentPos = 0;
    while (current) {
      ++currentPos;
      current = current.next;
      if (pos === 0) {
        this.head = current;
        this.head.prev = null;
        return true;
      } else {
        if (currentPos === pos) {
          if (current === this.tail) {
            current.prev.next = null;
            return true;
          } else {
            current.next.prev = current.prev;
            current.prev.next = current.next;
            return true;
          }
        }
      }
    }
    return false;
  }
}

const ll = new DoubledLinkedList(1);
ll.append(2);
ll.append(3);
ll.display();
ll.remove(2);
ll.display();
ll.appendAt(1, 22);
ll.display();
ll.appendAt(3, 56);
ll.display();
ll.appendAt(2, 99);
ll.display();
ll.traverse();
console.log('ll tail', ll.tail);
console.log('size', ll.size());
