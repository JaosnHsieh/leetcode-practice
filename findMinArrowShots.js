/**
 * 2019.09.29
 * refers to https://www.youtube.com/watch?v=DguJN47_mSg
 * @param {number[][]} points
 * @return {number}
 */
var findMinArrowShots = function(points) {
  if (points.length === 0) return 0;

  points.sort((a, b) => a[1] - b[1]);
  let right = points[0][1];
  let ans = 1;
  points.map(([p1, p2]) => {
    if (p1 > right) {
      right = p2;
      ++ans;
    }
  });
  return ans;
};
