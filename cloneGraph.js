/**
 *
 * 2019.08.09 came up with similar solution but didn't pass on leetcode
 * maybe it's leetcode platform bug
 * end up with modified solution refer to
 * https://leetcode.com/problems/clone-graph/discuss/350428/javascript-solution
 */
/**
 * // Definition for a Node.
 * function Node(val,neighbors) {
 *    this.val = val;
 *    this.neighbors = neighbors;
 * };
 */
/**
 * @param {Node} node
 * @return {Node}
 */

var cloneGraph = function(node) {
  let found = {};
  const cloneNode = n => {
    if (!n) {
      return n;
    }
    if(!found[n.val]){
      found[n.val] = new Node(n.val);
      found[n.val].neighbors = n.neighbors.map(nb => cloneNode(nb));
    }
    return found[n.val];
  };
  return cloneNode(node);
};


{"$id":"1","neighbors":[{"$id":"2","neighbors":[{"$ref":"1"},{"$id":"3","neighbors":[{"$ref":"2"},{"$id":"4","neighbors":[{"$ref":"3"},{"$ref":"1"}],"val":4}],"val":3}],"val":2},{"$ref":"4"}],"val":1}