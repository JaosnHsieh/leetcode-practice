var reverseList = function(head) {
  let current = head,
    prev = null,
    next = head.next;
  while (next) {
    next = current.next;
    current.next = prev;
    prev = current;
    current = next;
  }
  return prev;
};

console.log(
  reverseList({
    val: 1,
    next: {
      val: 2,
      next: null,
    },
  }),
);
