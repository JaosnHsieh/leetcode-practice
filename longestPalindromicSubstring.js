/** 2018.01.14
 * 2018.04.10 補充 這做法應該是參考 https://leetcode.com/problems/longest-palindromic-substring/solution/
 * Approach #4 (Expand Around Center) [Accepted]
 * @param {string} s
 * @return {string}
 */
var longestPalindrome = function(s) {
  var mostLength = 0;
  var startIndex = 0;

  for (var i = 0; i < s.length; ++i) {
    var left = 0,
      right = 0;
    if (s[i] === s[i + 1]) {
      left = i;
      right = i + 1;
      var pStr = searchP(s, left, right);
      if (pStr.length > mostLength) {
        startIndex = pStr.startIndex;
        mostLength = pStr.length;
      }
    }
    left = i;
    right = i;
    var pStr = searchP(s, left, right);
    if (pStr.length > mostLength) {
      startIndex = pStr.startIndex;
      mostLength = pStr.length;
    }
  }

  return s.substr(startIndex, mostLength);
};

function searchP(str, left, right) {
  var step = 1;
  while (left - step >= 0 && right + step <= str.length - 1) {
    if (str[left - step] !== str[right + step]) {
      break;
    }
    ++step;
  }
  var startIndex = left - step + 1;
  var length = right - left + 1 + (step - 1) * 2;
  return { startIndex, length };
}
