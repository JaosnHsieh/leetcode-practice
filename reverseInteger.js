/** 2018.01.16 寫這題 應該對的 但是leetcode噴錯
 * @param {number} x
 * @return {number}
 */
var reverse = function(x) {
    var isNegative = false;
    if(x<0){
        isNegative = true;
        x = Math.abs(x);
    }
    var reverseX = 0;
    var integerArr = [];
    while( x >= 10 || x <= -10 ){
        integerArr.push(x%10);
        x = Math.floor(x/10);
    }
    integerArr.push(x);
    for(var i = 0 ; i < integerArr.length   ; ++i ){
        var digits = integerArr.length -1 - i;
        reverseX += integerArr[i] * Math.pow(10, digits);
    }
    if(isNegative){
        reverseX *= -1;
    }

    return reverseX;
    
};
// var x = 1333;
// console.log(reverse(x));
//console.log(Math.pow(10,1));