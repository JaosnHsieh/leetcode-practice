/**
 * 2019.10.14
 * refers to
 * https://leetcode.com/problems/longest-common-prefix/discuss/6910/Java-code-with-13-lines
 */
var longestCommonPrefix = function(strs) {
  if (strs === null || strs.length === 0) {
    return '';
  }
  let pre = strs[0];
  for (let i = 1; i < strs.length; ++i) {
    while (strs[i].indexOf(pre) !== 0) {
      pre = pre.substring(0, pre.length - 1);
    }
  }
  return pre;
};

console.log(longestCommonPrefix(['flower', 'flow', 'flight']));
console.log(longestCommonPrefix(['c', 'acc', 'ccc']));
console.log(longestCommonPrefix(['b', 'cb', 'cab']));
