/**
 * 2019.09.17
 * refer to https://www.cnblogs.com/grandyang/p/4843654.html
 */
const findDuplicate = nums => {
  let min = 1,
    max = nums.length - 1,
    count = 0,
    middle = 0;

  while (min < max) {
    middle = Math.floor((min + max) / 2);

    for (let i = 0; i < nums.length; ++i) {
      if (nums[i] <= middle) {
        ++count;
      }
    }
    console.log('count', count);
    if (count <= middle) {
      min = middle + 1;
    } else {
      max = middle;
    }
    count = 0;
  }
  return max;
};
console.log(findDuplicate([1, 2, 3, 4, 2]));
0;
