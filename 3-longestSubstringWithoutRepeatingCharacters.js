//2018.04.09 refer to https://www.geeksforgeeks.org/length-of-the-longest-substring-without-repeating-characters/

var lengthOfLongestSubstring = function(s) {
  if (s.length === 0) {
    return 0;
  }
  let asciiMap = {};
  for (let i = 0; i <= 255; ++i) {
    asciiMap[i] = -1;
  }
  asciiMap[s[0].charCodeAt()] = 0;
  let prevIndex;
  let currentLength = 1;
  let maxLength = 1;
  for (let i = 1; i < s.length; ++i) {
    prevIndex = asciiMap[s[i].charCodeAt()];
    if (prevIndex === -1 || prevIndex < i - currentLength) {
      currentLength++;
    } else {
      if (currentLength > maxLength) {
        maxLength = currentLength;
      }
      currentLength = i - prevIndex;
    }
    asciiMap[s[i].charCodeAt()] = i;
  }
  if (currentLength > maxLength) {
    maxLength = currentLength;
  }
  return maxLength;
};
