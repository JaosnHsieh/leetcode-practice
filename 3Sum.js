/**
 * 2018.01.14 試圖暴力破解 但效率太差了
 * Submission Result: Time Limit Exceeded
 */


/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var threeSum = function(nums) {
    if(nums.length < 3){
        return [];
    }

    var arr = [];
    for(var i = 0 ; i < nums.length - 2 ; ++i){
        for(var j = i+1 ; j < nums.length - 1 ; ++j ){
            for(var k = j+1 ; k < nums.length ; ++k ){
                if(nums[i] + nums[j] + nums[k] === 0){
                    arr.push([nums[i], nums[j], nums[k]]);
                }
            }
        }
    }
    if( arr.length <= 1){
        return arr;
    }
    var newArr = [];
    newArr.push(arr[0]);
    // console.log('arr...',arr);
    // console.log(newArr);
    var allZero = false;

    outter:for(var i = 1 ; i < arr.length  ; ++i){
        if(arr[i][0] === 0 && arr[i][1] === 0 && arr[i][2] === 0){
            allZero = true;
            continue ;
       }

        var allSame = false;
       inner:for(var j = 0 ; j < newArr.length ; ++j){
           if((newArr[j].indexOf(arr[i][0]) !== -1 &&
           newArr[j].indexOf(arr[i][1])!==-1 &&
           newArr[j].indexOf(arr[i][2])!==-1 )){
               allSame = true;
           }
       }

       if(!allSame){
            newArr.push(arr[i]);
       }
    }
    if(allZero && !(newArr[0][0] === 0 && newArr[0][1] === 0  && newArr[0][2] === 0) ){
        newArr.push([0,0,0]);
    }

    return newArr;
};

var a = threeSum([3,0,3,2,-4,0,-3,2,2,0,-1,-5]);
console.log('a...',a);