/*
 * 參考這本書 Michael McMillan - Data sturcture and Algorithm with Javascript 寫的
 * https://github.com/JaosnHsieh/data_structures_and_algorithms_using_javascript/blob/master/Chapter12/Chap12-15.js
 */
const quickSort = arr => {
  if (arr.length <= 1) {
    return arr;
  }

  const pivot = arr[0];
  let left = [];
  let right = [];
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] < pivot) {
      left.push(arr[i]);
    } else {
      right.push(arr[i]);
    }
  }
  return quickSort(left)
    .concat([pivot])
    .concat(quickSort(right));
};

function test(arr) {
  const result = quickSort(arr);
  if (arr.length !== result.length) {
    throw new Error('length error');
  }
  for (let i = 0; i < result.length - 1; ++i) {
    if (result[i] > result[i + 1]) {
      console.log(`result[i] `, result[i]);
      console.log(`result[i + 1]`, result[i + 1]);
      console.log('i', i);
      throw new Error('sort error');
    }
  }
  console.log('passed');
}

const case1 = [1, 3, 2];
const case2 = [3, 2, 7, 5, 4, 3];
test(case1);
test(case2);
