/**
 *
 * 2019.10.13
 * brute force way with break optimal
 * refers to first 5 mins of this video
 * https://www.youtube.com/watch?time_continue=430&v=mtHelVTLKRQ
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/discuss/1729/11-line-simple-Java-solution-O(n)-with-explanation/224482
 */

var lengthOfLongestSubstring = function(s) {
  if (s.length === 0) {
    return 0;
  }
  let max = 1;
  for (let i = 0; i < s.length - 1; ++i) {
    let hash = {};
    hash[s[i]] = 1;
    for (let j = i + 1; j < s.length; ++j) {
      // console.log(
      //   `s.substring(i${i}, j${j}) ${s.substring(i, j)} s[j] ${s[j]}`,
      //   s.substring(i, j).indexOf(s[j]) === -1,
      // );
      if (hash[s[j]] === undefined) {
        max = Math.max(j - i + 1, max);
        hash[s[j]] = 1;
      } else {
        break;
      }
    }
  }
  return max;
};

console.log(lengthOfLongestSubstring('pwwkew'));
