/**
 * 2019.10.07 2 keys keyboard
 * refers to:
 * 2nd description
 * https://www.acwing.com/solution/LeetCode/content/502/
 * after finished this, still didn't get why prime sum is the answer
 *
 */

var minSteps = function(n) {
  const isPrime = n => {
    for (let i = 2; i <= Math.sqrt(n); ++i) {
      if (n % i === 0) {
        return false;
      }
    }
    return true;
  };

  function primeFactorier(n) {
    let arr = [];
    for (let i = 2; i <= n; ++i) {
      if (n % i === 0 && isPrime(i)) {
        arr.push(i);
        n = n / i;
        --i;
      }
    }
    return arr;
  }

  return primeFactorier(n).reduce((accu, current) => (accu += current), 0);
};

console.log(minSteps(2));
