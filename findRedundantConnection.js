/**
 *
 * 2019.09.30 refers to
 * https://www.youtube.com/watch?v=4hJ721ce010
 */
/**
 * @param {number[][]} edges
 * @return {number[]}
 */

var findRedundantConnection = function(edges) {
  class UnionFindSet {
    constructor(n) {
      this.parents = [];
      this.ranks = [];
      for (let i = 1; i <= n; ++i) {
        this.parents[i] = i;
        this.ranks[i] = 1;
      }
    }
    find(n) {
      if (n !== this.parents[n]) {
        this.parents[n] = this.find(this.parents[n]);
      }
      return this.parents[n];
    }
    union(x, y) {
      const rootX = this.find(x);
      const rootY = this.find(y);
      if (rootX === rootY) {
        return false;
      }

      if (this.ranks[rootX] > this.ranks[rootY]) {
        this.parents[rootY] = this.rootX;
      } else if (this.ranks[rootX] < this.ranks[rootY]) {
        this.parents[rootX] = this.rootY;
      } else {
        this.parents[rootX] = this.rootY;
        this.ranks[rootX]++;
      }

      this.parents[rootX] = rootY;
      return true;
    }
  }
  const set = new UnionFindSet(edges.length);
  for (let [x, y] of edges) {
    if (!set.union(x, y)) {
      return x < y ? [x, y] : [y, x];
    }
  }
};

console.log(findRedundantConnection([[1, 2], [1, 3], [2, 3]]));
