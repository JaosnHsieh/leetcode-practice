/**
 *
 * 2019.07.21 this solution came up by myself but before solved I referred to below solutions.
 * https://qiuyuntao.github.io/leetcode/solution/78.html
 * https://www.cnblogs.com/Liok3187/p/4733079.html
 *
 */
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var subsets = function(nums) {
  const results = [];

  const dfs = (currentArr, leftArr) => {
    if (leftArr.length === 0) {
      results.push(currentArr);
      return;
    }
    dfs([...currentArr, leftArr[0]], leftArr.slice(1));
    dfs([...currentArr], leftArr.slice(1));
  };
  dfs([], nums.slice(0));
  return results;
};
console.log(subsets([1, 2, 3]));
