/**
 *
 * 2019.10.13
 * brute force way with break optimal
 * refers to last 5 mins of this video
 * https://www.youtube.com/watch?time_continue=430&v=mtHelVTLKRQ
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/discuss/1729/11-line-simple-Java-solution-O(n)-with-explanation/224482
 */

var lengthOfLongestSubstring = function(s) {
  if (s === null || s.length === 0) {
    return 0;
  }
  if (s.length === 1) {
    return 1;
  }

  let set = new Set();
  let max = 0;
  let left = 0;
  let right = 0;
  while (right <= s.length - 1) {
    if (set.has(s[right])) {
      set.delete(s[left]);
      ++left;
    } else {
      set.add(s[right]);
      max = Math.max(max, right - left + 1);
      ++right;
    }
  }
  return max;
};

console.log(lengthOfLongestSubstring('pwwkew'));
