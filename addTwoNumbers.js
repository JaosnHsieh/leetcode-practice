// translated JAVA code to javascript refer to https://leetcode.com/articles/add-two-numbers/

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2) {
  let head = new ListNode(0);
  let p = l1;
  let q = l2;
  let current = head;
  let carry = 0;
  while (p !== null || q !== null) {
    let x = p === null ? 0 : p.val;
    let y = q === null ? 0 : q.val;
    let sum = x + y + carry;
    carry = Math.floor(sum / 10);
    current.next = new ListNode(sum % 10);
    current = current.next;
    if (p !== null) p = p.next;
    if (q !== null) q = q.next;
  }
  if (carry > 0) {
    current.next = new ListNode(carry);
  }
  return head.next;
};
