/**
 * @param {number} n
 * @return {string[]}
 */
var generateParenthesis = function(n) {
  let ans = [];
  let map = {};

  const isPossible = (str = '', n = 0) => {
    if (map[str] !== undefined) {
      return map[str];
    }
    let left = 0;
    let right = 0;
    for (let i = 0; i < str.length; ++i) {
      if (str[i] === '(') {
        ++left;
      }
      if (str[i] === ')') {
        ++right;
      }
      console.log(`left ${left}, n ${n}`);
      console.log(`left > n`, left > n);
      if (right > left || left > n) {
        map[str] = false;
        return false;
      }
    }
    map[str] = true;
    return true;
  };
  const listAll = (str = '', number = 0) => {
    // console.log(`str.length`, str.length, `number * 2`, number * 2);
    if (str.length === number * 2 && isPossible(str, number)) {
      return ans.push(str);
    }
    if (isPossible(str, number)) {
      listAll(`${str}(`, number);
      listAll(`${str})`, number);
    }
  };

  listAll('', n);
  return ans;
};
