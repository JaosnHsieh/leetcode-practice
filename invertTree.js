/**
 * 2019.09.30
 * refers to
 * [coding interview] Invert Binary Tree
 * https://www.youtube.com/watch?v=PB6_BKvH5xA
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var invertTree = function(root) {
  const dfs = node => {
    if (!node) {
      return;
    }
    // console.log('node', node);
    swap(node);
    dfs(node.left);
    dfs(node.right);
  };
  function swap(node) {
    const temp = node.left;
    node.left = node.right;
    node.right = temp;
  }
  dfs(root);
  return root;
};
