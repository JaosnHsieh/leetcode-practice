// 2019.01.30  https://leetcode.com/problems/letter-case-permutation
// refer to 花花酱 LeetCode 784. Letter Case Permutation - 刷题找工作 EP169
// https://www.youtube.com/watch?v=LJifc-ehvBM

 


/**
 * @param {string} S
 * @return {string[]}
 */
let count = 0 ;
var letterCasePermutation = function(S) {
    
    const dfs = (s = '', position = 0, ans = [])=>{
        // ++count;
        // if(count===5)return;
        // console.log(s);
        if(position===S.length){
            
            ans.push(s);
            return;
        }
        
        if(/[a-z]/.test(S[position])){
            dfs(s+S[position].toUpperCase(),position+1,ans);
            dfs(s+S[position],position+1,ans);
        }else if(/[A-Z]/.test(S[position])){
            dfs(s+S[position],position+1,ans);
            dfs(s+S[position].toLowerCase(),position+1,ans);
        }else{
            dfs(s+S[position],position+1,ans)
        }
    }
    let ans = [];
    dfs(s='',0,ans);
    return ans;
    
};
console.log(letterCasePermutation('1A2z'))