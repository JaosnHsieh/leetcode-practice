/**
 * lruCache3.js
 * 2019.09.27
 * practice again from scratch.
 * passed on leetcode but not as efficient as lruCache2.js probably due to new Class is more memory consuming than literal object.
 */
class Node {
  constructor({ value, key, next, prev }) {
    this.value = value || null;
    this.key = key || null;
    this.next = next || null;
    this.prev = prev || null;
  }
}
class DoubleLinkedList {
  constructor() {
    let dummyHead = new Node({ value: null, key: null, next: null, prev: null });
    let dummyTail = new Node({ value: null, key: null, next: null, prev: null });
    dummyHead.next = dummyTail;
    dummyTail.prev = dummyHead;
    this.head = dummyHead;
    this.tail = dummyTail;
  }
  addToHead(node) {
    node.next = this.head.next;
    node.prev = this.head;
    this.head.next.prev = node;
    this.head.next = node;
  }
  deleteTail() {
    const tailKey = this.tail.prev.key;
    this.tail.prev.prev.next = this.tail;
    this.tail.prev = this.tail.prev.prev;
    return tailKey;
  }
  traverse() {
    //for testing
    let current = this.head;
    while (current) {
      console.log(current.value);
      current = current.next;
    }
  }
}
// const dll = new DoubleLinkedList();
// dll.addToHead(new Node({ value: 1 }));
// dll.addToHead(new Node({ value: 2 }));
// dll.addToHead(new Node({ value: 3 }));
// dll.deleteTail();
// dll.traverse();

class LRUCache {
  constructor(capacity) {
    this.capacity = capacity;
    this.count = 0;
    this.map = {};
    this.dll = new DoubleLinkedList({});
  }
  cutNodeLinksFromDll(node) {
    node.prev.next = node.next;
    node.next.prev = node.prev;
  }
  addNewNode({ value, key }) {
    const node = new Node({ value, key });
    this.dll.addToHead(node);
    this.map[key] = node;
    ++this.count;
  }
  put(key, value) {
    if (this.map[key] !== undefined) {
      // console.log(`this.map[key]`, this.map[key]);
      this.cutNodeLinksFromDll(this.map[key]);
      --this.count;
      delete this.map[key];
      this.addNewNode({ value, key });
    } else {
      if (this.count >= this.capacity) {
        const tailkey = this.dll.deleteTail();
        delete this.map[tailkey];
        --this.count;
      }
      this.addNewNode({ value, key });
    }
  }
  get(key) {
    if (this.map[key] !== undefined) {
      const node = this.map[key];
      this.cutNodeLinksFromDll(node);
      const newNode = new Node({ value: node.value, key });
      this.map[key] = newNode;
      this.dll.addToHead(newNode);
      return node.value;
    }
    return -1;
  }
}

const cache = new LRUCache(2 /* capacity */);
console.log(undefined);
console.log(cache.get(2));
console.log(cache.put(2, 6));
console.log(cache.get(1));
console.log(cache.put(1, 5));
console.log(cache.put(1, 2));
console.log(cache.get(1));
console.log(cache.get(2));
// Input
// ["LRUCache","get","put","get","put","put","get","get"]
// [[2],[2],[2,6],[1],[1,5],[1,2],[1],[2]]
// Output
// [null,-1,null,-1,null,null,2,-1]
// Expected
// [null,-1,null,-1,null,null,2,6]
