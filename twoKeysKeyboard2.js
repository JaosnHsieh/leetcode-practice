/**
 * 2019.10.08
 * this easy, amazing solution comes from
 * https://leetcode.com/problems/2-keys-keyboard/discuss/105908/Very-Simple-Java-Solution-With-Detail-Explanation
 *
 * understandable, easy and efficient.
 *
 */

/**
 * @param {number} n
 * @return {number}
 */
var minSteps = function(n) {
  let res = 0;
  for (let i = 2; i <= n; ++i) {
    while (n % i === 0) {
      res += i;
      n = n / i;
    }
  }
  return res;
};

console.log(minSteps(5));
