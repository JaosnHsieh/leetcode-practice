/**
 * @param {number[]} nums
 * @return {number[][]}
 */

var twoSum = function(nums, target) {
    var arr = [];
    var map = {};
    for(var i = 0 ; i < nums.length ; ++i){
        map[nums[i]] = i;
    }

    for(var i = 0 ; i < nums.length ; ++i){
        if(map[ target - nums[i]] && i !== map[ target - nums[i]]){
            arr.push([i , map[ target - nums[i]]]);
        }
    }
    return arr;
};

console.log(twoSum([3,0,1,-3],0));