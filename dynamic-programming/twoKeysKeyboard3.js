/**
 * 2019.10.10
 */

// decompose example for n === 6
// var minSteps = function(n) {
//   let dp = [];
//   dp[1] = 0;

//   for (let i = 2; i <= n; ++i) {
//     let result = Number.MAX_SAFE_INTEGER;

//     if (i % 2 === 0) {
//       result = Math.min(result, dp[i / 2] + 2);
//     }
//     if (i % 3 === 0) {
//       result = Math.min(result, dp[i / 3] + 3);
//     }
//     if (i % 4 === 0) {
//       result = Math.min(result, dp[i / 4] + 4);
//     }
//     if (i % 5 === 0) {
//       result = Math.min(result, dp[i / 5] + 5);
//     }
//     if (i % 6 === 0) {
//       result = Math.min(result, dp[i / 6] + 6);
//     }
//     dp[i] = result;
//     // for (let j = i; j <= i; ++j) {
//     //   if (i % j === 0) {
//     //     result = Math.min(result, dp[i / j] + j);
//     //     console.log('result', result);
//     //   }
//     // }
//     // dp[i] = result;
//   }
//   //   console.log(dp);
//   return dp[n];
// };

// var minSteps = function(n) {
//   let dp = [];
//   dp[1] = 0;

//   for (let i = 2; i <= n; ++i) {
//     let result = Number.MAX_SAFE_INTEGER;
//     for (let j = 2; j <= i; ++j) {
//       if (i % j === 0) {
//         result = Math.min(result, dp[i / j] + j);
//       }
//     }
//     dp[i] = result;
//   }
//   return dp[n];
// };

// //answer come up by myself but slow ( 15% )
// var minSteps = function(n) {
//   let dp = [0, 0];

//   for (let i = 2; i <= n; ++i) {
//     if (dp[i] !== undefined) {
//       continue;
//     }
//     let result = Number.MAX_SAFE_INTEGER;
//     for (let j = 2; j <= i; ++j) {
//       if (i % j === 0) {
//         result = Math.min(result, dp[i / j] + j);
//       }
//     }
//     dp[i] = result;
//   }
//   return dp[n];
// };

//answer see from details fastest with cache ( 90% up )

var minSteps = function(n) {
  const dp = [0];

  function recurse(n) {
    if (dp[n] !== undefined) return dp[n];

    dp[n] = n;
    for (let i = 1; i <= n; ++i) {
      if (n % i === 0) {
        dp[n] = Math.min(dp[n], recurse(n / i, dp) + i);
      }
    }
    return dp[n];
  }

  return recurse(n);
};

console.log(minSteps(6));
