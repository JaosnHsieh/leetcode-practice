/**
 * 2019.10.12
 * https://leetcode.com/problems/maximum-length-of-repeated-subarray/discuss/279167/Javascript-DP-solution
 */

/**
 * @param {number[]} A
 * @param {number[]} B
 * @return {number}
 */
var findLength = function(A, B) {
  const dp = Array(A.length + 1)
    .fill(0)
    .map(() => Array(B.length + 1).fill(0));

  let maxLength = 0;
  for (let i = 1; i <= A.length; i++) {
    for (let j = 1; j <= B.length; j++) {
      if (A[i - 1] == B[j - 1]) {
        // console.log(`i${i - 1},j${j - 1}`);
        dp[i][j] = 1 + dp[i - 1][j - 1];
        if (dp[i][j] > maxLength) {
          maxLength = dp[i][j];
        }
      }
    }
  }
  return maxLength;
};

console.log(findLength([1, 2, 3, 2, 1], [3, 2, 1, 4, 7]));
