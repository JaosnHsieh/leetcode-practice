/**
 *
 * 2019.10.12
 * https://www.youtube.com/watch?v=7J5rs56JBs8
 */
/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function(nums) {
  // start with the first number and at each stage decide whether it makes sense to keep the previous chain or restart
  let currentSum = nums[0];
  let maxSum = nums[0];

  for (let i = 1; i < nums.length; ++i) {
    currentSum = Math.max(nums[i], currentSum + nums[i]);
    maxSum = Math.max(maxSum, currentSum);
  }

  return maxSum;
};

console.log(maxSubArray([1, 2, -3, 4, 5]));
