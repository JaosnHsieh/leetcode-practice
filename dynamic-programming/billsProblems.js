/**
 *
 * dynamic programming 動態規劃
 * so far I found on the internet,
 * the most understandable description of dynamic programming.
 * using bills problem as an example.
 * https://www.zhihu.com/question/23995189/answer/613096905
 * https://copyfuture.com/blogs-details/201907281925220750mjfuh3uobp8yqg
 */

/***
 * original, easy to understand version
 */
let bills = [1, 5, 11];
let n = 15;
let dp = [];

dp[0] = 0;

for (let i = 1; i <= n; ++i) {
  let cost = Number.MAX_SAFE_INTEGER;
  if (i - 1 >= 0) {
    cost = Math.min(cost, dp[i - 1] + 1);
  }
  if (i - 5 >= 0) {
    cost = Math.min(cost, dp[i - 5] + 1);
  }
  if (i - 11 >= 0) {
    cost = Math.min(cost, dp[i - 11] + 1);
  }
  dp[i] = cost;
}

/**
 * pretty version
 */
// let bills = [1, 5, 11];
// let n = 24;
// let dp = [];

// dp[0] = 0;

// for (let i = 1; i <= n; ++i) {
//   let cost = Number.MAX_SAFE_INTEGER;
//   for (let j of bills) {
//     if (i - j >= 0) {
//       cost = Math.min(cost, dp[i - j]) + 1;
//     }
//     dp[i] = cost;
//   }
// }

console.log(dp[n]);
