/**
 * this one followed same pattern as ./biilsProblems.js
 */

const itemsWeigh = [4, 2, 3];
const values = [10, 4, 7];
const backpackCapacity = 6;

let dp = [];

dp[0] = 0;

for (let i = 1; i <= backpackCapacity; ++i) {
  let value = 0;
  if (i - 4 >= 0) {
    value = Math.max(value, dp[i - 4] + 10);
  }
  if (i - 2 >= 0) {
    value = Math.max(value, dp[i - 2] + 4);
  }
  if (i - 3 >= 0) {
    value = Math.max(value, dp[i - 3] + 7);
  }

  dp[i] = value;
}

console.log(dp[backpackCapacity]);
