/**
 * refers to
 * 花花酱 Disjoint-set/Union-find Forest - 刷题找工作 SP1
 * https://www.youtube.com/watch?v=VJnUwsE4fWA&t=983s
 */

/**
 * @param {number[][]} M
 * @return {number}
 */
class UnionFindSet {
  constructor(n = 0) {
    this.ranks = Array(n);
    this.parents = Array(n);
    // this.ranks = [];
    for (let i = 0; i < n; ++i) {
      this.parents[i] = i;
      this.ranks[i] = 1;
    }
  }

  find(x) {
    if (this.parents[x] !== x) {
      this.parents[x] = this.find(this.parents[x]);
    }
    return this.parents[x];
  }

  union(x, y) {
    const rootX = this.find(x);
    const rootY = this.find(y);
    if (rootX === rootY) {
      return false;
    }
    this.parents[rootX] = this.parents[rootY];
    return true;
  }
}
var findCircleNum = function(M) {
  const set = new UnionFindSet(M.length);

  for (let i = 0; i < M.length; ++i) {
    for (let j = i + 1; j < M.length; ++j) {
      if (M[i][j] === 1) {
        set.union(i, j);
      }
    }
  }

  const circleSet = new Set();
  for (let i = 0; i < M.length; ++i) {
    circleSet.add(set.find(i));
  }
  return circleSet.size;
};

console.log(findCircleNum([[1, 1, 0], [1, 1, 0], [0, 0, 1]]));
