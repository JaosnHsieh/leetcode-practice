/**
 * refer to Computer science in JavaScript: Quicksort
 * Posted at November 27, 2012 by Nicholas C. Zakas
 * https://humanwhocodes.com/blog/2012/11/27/computer-science-in-javascript-quicksort/
 * my thought after reading the article:
 * the pivot index return by partition function is confusing, it will return index value either the pivot value position is, or the next position of pivot value,
 * but it is still get the right result because even put the pivot value in the left sort, it doesn't affect the result.
 * personally, I prefer the quicksort.js in Michael McMillan - Data sturcture and Algorithm with Javascript, simpler but cost more memory than this one.
 */

function swap(items, firstIndex, secondIndex) {
  var temp = items[firstIndex];
  items[firstIndex] = items[secondIndex];
  items[secondIndex] = temp;
}

function partition(items, left, right) {
  var pivot = items[Math.floor((right + left) / 2)],
    i = left,
    j = right;

  while (i <= j) {
    while (items[i] < pivot) {
      i++;
    }

    while (items[j] > pivot) {
      j--;
    }

    if (i <= j) {
      swap(items, i, j);
      i++;
      j--;
    }
  }

  return i;
}

function quickSort(items, left, right) {
  var index;

  if (items.length > 1) {
    index = partition(items, left, right);

    //

    if (left < index - 1) {
      quickSort(items, left, index - 1);
    }

    if (index < right) {
      quickSort(items, index, right);
    }
  }

  return items;
}

function test(arr) {
  const result = quickSort(arr, 0, arr.length - 1);
  console.log('result', result);
  if (arr.length !== result.length) {
    throw new Error('length error');
  }
  for (let i = 0; i < result.length - 1; ++i) {
    if (result[i] > result[i + 1]) {
      console.log(`result[i] `, result[i]);
      console.log(`result[i + 1]`, result[i + 1]);
      console.log('i', i);
      throw new Error('sort error');
    }
  }
  console.log('passed');
}

const case2 = [3, 2, 7, 5, 4, 3];
test(case2);
