/**
 *
 * 2019.09.30
 * clearest description on the internet
 * https://buptwc.com/2018/11/25/Leetcode-947-Most-Stones-Removed-with-Same-Row-or-Column/
 * from: https://leetcode.com/problems/most-stones-removed-with-same-row-or-column/discuss/200034/Someone-please-help-I-just-don't-get-this
 *
 */
/**
 * @param {number[][]} stones
 * @return {number}
 */

class UnionFindSet {
  constructor(n = 0) {
    this.parents = Array(n);
    this.ranks = Array(n);
    for (let i = 0; i < n; ++i) {
      this.parents[i] = i;
      this.ranks[i] = 1;
    }
  }
  find(x) {
    if (x !== this.parents[x]) {
      this.parents[x] = this.find(this.parents[x]);
    }
    return this.parents[x];
  }
  union(x, y) {
    const rootX = this.find(x);
    const rootY = this.find(y);
    if (rootX === rootY) {
      return false;
    }
    if (this.ranks[x] === this.ranks[y]) {
      this.parents[rootX] = this.parents[rootY];
      this.ranks[x]++;
    }
    this.parents[rootX] = this.parents[rootY];
    return true;
  }
}
var removeStones = function(stones) {
  let set = new UnionFindSet(stones.length);
  for (let i = 0; i < stones.length; ++i) {
    for (let j = 1; j < stones.length; ++j) {
      const [x1, y1] = stones[i];
      const [x2, y2] = stones[j];
      if (x1 === x2 || y1 === y2) {
        set.union(i, j);
      }
    }
  }
  let uniqueSet = new Set();
  for (let i = 0; i < stones.length; ++i) {
    uniqueSet.add(set.find(i));
  }
  return stones.length - uniqueSet.size;
};

const stones = [[0, 0], [0, 1], [1, 0], [1, 2], [2, 1], [2, 2]];
console.log(`output ${removeStones(stones)}`);
