// references
// http://bangbingsyb.blogspot.com/2014/11/leetcode-permutation-sequence.html
// https://github.com/paopao2/leetcode-js/blob/master/Permutation%20Sequence.js

const getPermutation = (n, k) => {
  let arr = [],
    numPositions = [],
    i = 0,
    result = "";
  for (let q = 0; q < n; ++q) {
    arr[q] = q + 1;
    numPositions[q] = 0;
  }

  if (k === 1) {
    return arr.join("");
  }

  while (k > 0 && n > 1) {
    const f = getFactorial(n - 1);
    // 這裡是照喜刷刷的邏輯去想的 但如果用 reference 中 github 那個 k = k-1 會簡單很多
    numPositions[i] = k / f >= 1 ? Math.ceil(k / f) - 1 : 1 - 1;
    k = k % f === 0 ? f : k % f;
    n--;
    i++;
  }

  for (let q = 0; q < numPositions.length; ++q) {
    result += arr[numPositions[q]];
    arr.splice(numPositions[q], 1);
  }
  return result;
};

function getFactorial(n) {
  let result = 1;
  if (n === 0) {
    return 0;
  }

  while (n > 1) {
    result *= n;
    n--;
  }
  return result;
}

console.log(getPermutation(4, 9));
